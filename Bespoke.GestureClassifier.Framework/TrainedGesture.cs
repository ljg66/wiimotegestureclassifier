﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using Bespoke.Common;
using Bespoke.Common.LinearAlgebra;
using System.Collections.Concurrent;

namespace Bespoke.GestureClassifier.Framework
{
    public class TrainedGesture : IComparable<TrainedGesture>
    {
        #region Properties

        public string GestureName { get; set; }

        public PlayerIndex PlayerIndex { get; set; }

        public ReadOnlyCollection<Gesture> Samples => mSamples.AsReadOnly();

        public Dictionary<Gesture.Features, double> Weights { get; private set; }

        public double InitialWeight { get; private set; }

        #endregion

        public TrainedGesture(string gestureName)
            : this(gestureName, PlayerIndex.One)
        {
        }

        public TrainedGesture(string gestureName, PlayerIndex playerIndex)
        {
            GestureName = gestureName;
            PlayerIndex = playerIndex;
            mSamples = new GestureCollection();
            mFeatureVectorNeedsCalculation = true;
            mCovarianceMatrixNeedsCalculation = true;
        }

        #region Public Methods

        public static async Task<TrainedGesture> LoadAsync(BinaryReader reader)
        {
            string gestureName = reader.ReadString();
            PlayerIndex playerIndex = (PlayerIndex)Enum.Parse(typeof(PlayerIndex), reader.ReadString());
            TrainedGesture trainedGesture = new TrainedGesture(gestureName, playerIndex);

            GestureCollection samples = await GestureCollection.LoadAsync(reader);
            foreach (Gesture sample in samples)
            {
                trainedGesture.AddSample(sample);
            }

            return trainedGesture;
        }

        public async Task SaveAsync(BinaryWriter writer)
        {
            writer.Write(GestureName);
            writer.Write(PlayerIndex.ToString());
            await mSamples.SaveAsync(writer);
        }

        public void AddSample(Gesture sample)
        {
            Assert.ParamIsNotNull(nameof(sample), sample);
            Assert.IsTrue(nameof(sample), sample.Points.Length > 0);

            mSamples.Add(sample);
            mFeatureVectorNeedsCalculation = true;
            mCovarianceMatrixNeedsCalculation = true;
        }

        public void RemoveSample(Gesture sample)
        {
            Assert.ParamIsNotNull(nameof(sample), sample);
            mSamples.Remove(sample);
        }

        public Dictionary<Gesture.Features, double> GetFeatureVector()
        {
            if (mFeatureVectorNeedsCalculation)
            {
                mFeatureVector = new Dictionary<Gesture.Features, double>();

                if (mSamples.Count > 0)
                {
                    // Collect the features from each sample and compute the average
                    foreach (Gesture.Features feature in (Gesture.Features[])Enum.GetValues(typeof(Gesture.Features)))
                    {
                        double averageValue = (from sample in mSamples
                                               select Gesture.GetFeatureValueFromSample(feature, sample)).Average();

                        mFeatureVector.Add(feature, averageValue);
                    }
                }

                mFeatureVectorNeedsCalculation = false;
            }

            return mFeatureVector;
        }

        public Dictionary<Gesture.Features, double> GetFeatureVariances()
        {
            Dictionary<Gesture.Features, double> featureVariances = new Dictionary<Gesture.Features, double>();

            Dictionary<Gesture.Features, double> featureVector = GetFeatureVector();
            foreach (Gesture.Features feature in featureVector.Keys)
            {
                double variance = (from sample in mSamples
                                   select Gesture.GetFeatureValueFromSample(feature, sample) - featureVector[feature]).
                                   Aggregate((aggregate, value) => aggregate += value * value);

                featureVariances.Add(feature, variance);
            }

            return featureVariances;
        }

        public Matrix GetCovarianceMatrix()
        {
            if (mCovarianceMatrixNeedsCalculation)
            {
                Dictionary<Gesture.Features, double> featureVector = GetFeatureVector();
                mCovarianceMatrix = new Matrix(featureVector.Keys.Count, featureVector.Keys.Count);

                int i = 0;
                foreach (Gesture.Features iFeature in featureVector.Keys)
                {
                    double iAverageValue = featureVector[iFeature];

                    int j = 0;
                    foreach (Gesture.Features jFeature in featureVector.Keys)
                    {
                        double jAverageValue = featureVector[jFeature];
                        double covariance = 0.0;

                        foreach (Gesture sample in mSamples)
                        {
                            double iValue = Gesture.GetFeatureValueFromSample(iFeature, sample);
                            double jValue = Gesture.GetFeatureValueFromSample(jFeature, sample);

                            covariance += (iValue - iAverageValue) * (jValue - jAverageValue);

                            if (double.IsNaN(covariance))
                            {
                                throw new Exception(string.Format("NaN found in covariance matrix at feature: [{0}][{1}", iFeature, jFeature));
                            }
                        }

                        mCovarianceMatrix[i, j] = covariance;

                        j++;
                    }

                    i++;
                }

                mCovarianceMatrixNeedsCalculation = false;
            }

            return mCovarianceMatrix;
        }

        public Dictionary<Gesture.Features, double> SetWeights(Matrix invertedCommonCovarianceMatrix)
        {
            Weights = new Dictionary<Gesture.Features, double>();
            Dictionary<Gesture.Features, double> featureVector = GetFeatureVector();
            Gesture.Features[] features = (Gesture.Features[])Enum.GetValues(typeof(Gesture.Features));
            if (featureVector.Count != features.Length)
            {
                throw new Exception("Feature vector has an invalid set of features.");
            }

            int j = 0;
            foreach (Gesture.Features jFeature in features)
            {
                double weight = 0.0;
                int i = 0;
                foreach (Gesture.Features iFeature in features)
                {
                    weight += invertedCommonCovarianceMatrix[i, j] * featureVector[iFeature];
                    i++;
                }

                Weights.Add(jFeature, weight);
                j++;
            }

            double initialWeightFactor = (from feature in (Gesture.Features[])Enum.GetValues(typeof(Gesture.Features))
                                          select Weights[feature] * featureVector[feature]).Sum();

            InitialWeight = initialWeightFactor / (-2.0);

            return Weights;
        }

        public override string ToString() => GestureName;

        public int CompareTo(TrainedGesture other) => GestureName.CompareTo(other.GestureName);

        public static bool operator ==(TrainedGesture lhs, TrainedGesture rhs)
        {
            if (ReferenceEquals(lhs, rhs))
            {
                return true;
            }

            if ((object)lhs == null || (object)rhs == null)
            {
                return false;
            }

            return (lhs.GestureName == rhs.GestureName);
        }

        public static bool operator !=(TrainedGesture lhs, TrainedGesture rhs) => !(lhs == rhs);

        public override bool Equals(object obj)
        {
            TrainedGesture rhs = obj as TrainedGesture;
            if ((object)rhs == null)
            {
                return false;
            }

            return this == rhs;
        }

        public override int GetHashCode() => GestureName.GetHashCode();

        #endregion

        private GestureCollection mSamples;
        private Dictionary<Gesture.Features, double> mFeatureVector;
        private Matrix mCovarianceMatrix;
        private bool mFeatureVectorNeedsCalculation;
        private bool mCovarianceMatrixNeedsCalculation;
    }

    public class TrainedGestureCollection : ObservableCollection<TrainedGesture>
    {
        public PlayerIndex PlayerIndex { get; set; }

        public TrainedGesture this[string gestureName] => (from trainedGesture in this
                                                           where trainedGesture.GestureName == gestureName
                                                           select trainedGesture).FirstOrDefault();

        public TrainedGestureCollection(PlayerIndex playerIndex)
        {
            PlayerIndex = playerIndex;
        }

        public static async Task<TrainedGestureCollection> LoadAsync(string fileName, PlayerIndex playerIndex)
        {
            if (File.Exists(fileName) == false)
            {
                throw new FileNotFoundException();
            }

            using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                return await LoadAsync(new BinaryReader(fileStream), playerIndex);
            }
        }

        public static async Task<TrainedGestureCollection> LoadAsync(byte[] trainingData, PlayerIndex playerIndex)
        {
            Assert.ParamIsNotNull(nameof(trainingData), trainingData);

            using (MemoryStream memoryStream = new MemoryStream(trainingData))
            {
                return await LoadAsync(new BinaryReader(memoryStream), playerIndex);
            }
        }

        public static async Task<TrainedGestureCollection> LoadAsync(BinaryReader reader, PlayerIndex playerIndex)
        {
            TrainedGestureCollection trainedGestures = new TrainedGestureCollection(playerIndex);

            int totalTrainedGestures = reader.ReadInt32();

            for (int i = 0; i < totalTrainedGestures; i++)
            {
                trainedGestures.Add(await TrainedGesture.LoadAsync(reader));
            }

            return trainedGestures;
        }

        public async Task SaveAsync(string fileName)
        {
            using (FileStream file = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                BinaryWriter writer = new BinaryWriter(file);
                writer.Write(Count);

                foreach (TrainedGesture trainedGesture in this)
                {
                    await trainedGesture.SaveAsync(writer);
                }
            }
        }

        public async Task SaveAsync(BinaryWriter writer)
        {
            writer.Write(Count);

            foreach (TrainedGesture trainedGesture in this)
            {
                await trainedGesture.SaveAsync(writer);
            }
        }

        public Matrix GetCommonCovarianceMatrix()
        {
            int featureCount = Enum.GetValues(typeof(Gesture.Features)).Length;
            Matrix commonCovarianceMatrix = new Matrix(featureCount, featureCount);

            if (Count > 0)
            {
                for (int i = 0; i < commonCovarianceMatrix.rows; i++)
                {
                    for (int j = 0; j < commonCovarianceMatrix.cols; j++)
                    {
                        // Collect the covariance from each trained gesture
                        double totalCovariance = 0.0;
                        int totalSampleCount = 0;
                        foreach (TrainedGesture trainedGesture in this)
                        {
                            Matrix covarianceMatrix = trainedGesture.GetCovarianceMatrix();
                            if (covarianceMatrix.Data.GetLength(0) > i && covarianceMatrix.Data.GetLength(1) > j)
                            {
                                totalCovariance += covarianceMatrix[i, j];
                            }
                            totalSampleCount += trainedGesture.Samples.Count;
                        }

                        // Average the covariances
                        commonCovarianceMatrix[i, j] = totalCovariance / (totalSampleCount - Count);
                    }
                }
            }

            return commonCovarianceMatrix;
        }

        public void SetWeights()
        {
            Matrix invertedCommonCovarianceMatrix = GetCommonCovarianceMatrix().Inverse();

            for (int row = 0; row < invertedCommonCovarianceMatrix.rows; row++)
            {
                for (int col = 0; col < invertedCommonCovarianceMatrix.cols; col++)
                {
                    if (double.IsNaN(invertedCommonCovarianceMatrix.Data[row, col]))
                    {
                        throw new Exception("Invalid Common Covariance Matrix");
                    }
                }
            }

            foreach (TrainedGesture trainedGesture in this)
            {
                trainedGesture.SetWeights(invertedCommonCovarianceMatrix);
            }
        }

        public Task SetWeightsAsync() => Task.Run(() => SetWeights());
    }
}
