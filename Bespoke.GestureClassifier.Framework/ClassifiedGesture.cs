﻿using System;
using System.Collections.Generic;

namespace Bespoke.GestureClassifier.Framework
{
    public class ClassifiedGesture : IComparable<ClassifiedGesture>
    {
        public string GestureName { get; }

        public double Probability { get; }

        public ClassifiedGesture(string gestureName, double probability)
        {
            GestureName = gestureName;
            Probability = probability;
        }

        public int CompareTo(ClassifiedGesture other) => -Probability.CompareTo(other.Probability);
    }

    public class ClassifiedGestureCollection : List<ClassifiedGesture>
    {
    }
}
