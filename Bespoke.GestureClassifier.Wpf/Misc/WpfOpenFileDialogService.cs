﻿using Microsoft.Win32;
using Bespoke.Common;

namespace Bespoke.GestureClassifier.Wpf
{
    public class WpfOpenFileDialogService : IOpenFileDialogService
    {
        public string DefaultExt
        {
            get
            {
                return mOpenFileDialog.DefaultExt;
            }
            set
            {
                mOpenFileDialog.DefaultExt = value;
            }
        }

        public string Filter
        {
            get
            {
                return mOpenFileDialog.Filter;
            }
            set
            {
                mOpenFileDialog.Filter = value;
            }
        }

        public bool RestoreDirectory
        {
            get
            {
                return mOpenFileDialog.RestoreDirectory;
            }
            set
            {
                mOpenFileDialog.RestoreDirectory = value;
            }
        }

        public string FileName
        {
            get
            {
                return mOpenFileDialog.FileName;
            }
            private set
            {
                mOpenFileDialog.FileName = value;
            }
        }

        public WpfOpenFileDialogService()
        {
            mOpenFileDialog = new OpenFileDialog();
        }

        public bool? ShowDialog() => mOpenFileDialog.ShowDialog();

        private OpenFileDialog mOpenFileDialog;
    }
}
