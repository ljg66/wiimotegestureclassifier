﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Microsoft.Practices.Unity;
using Prism.Unity;
using Bespoke.Common;
using Bespoke.GestureClassifier.Wpf.ViewModels;
using WiimoteLib;

namespace Bespoke.GestureClassifier.Wpf
{
    public class Bootstrapper : UnityBootstrapper
    {
        protected override DependencyObject CreateShell() => Container.Resolve<MainWindow>();

        protected override void InitializeShell() => Application.Current.MainWindow.Show();

        protected override void ConfigureContainer()
        {
            WiimoteCollection wiimoteCollection = new WiimoteCollection();

            try
            {
                wiimoteCollection.FindAllWiimotes();
            }
            catch (WiimoteNotFoundException ex)
            {
                Trace.WriteLine(string.Format("Wiimote not found.\n{0}", ex.Message));
            }
            catch (WiimoteException ex)
            {
                Trace.WriteLine(string.Format("Wiimote error.\n{0}", ex.Message));
            }
            catch (Exception ex)
            {
                Trace.WriteLine(string.Format("Unknown error.\n{0}", ex.Message));
            }

            try
            {
                for (int i = 0; i < wiimoteCollection.Count; i++)
                {
                    PlayerIndex playerIndex = (PlayerIndex)i;
                    Wiimote wiimote = wiimoteCollection[i];

                    wiimote.Connect();
                    wiimote.SetReportType(InputReport.IRAccel, true);
                    wiimote.SetLEDs(i + 1);
                    Container.RegisterInstance<Wiimote>(playerIndex.ToString(), wiimote);
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(string.Format("Wiimote connection failure.\n{0}", ex.Message));
            }

            Container.RegisterType<MainWindowViewModel>();
            Container.RegisterType<IMessageBoxService, WpfMessageBoxService>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IOpenFileDialogService, WpfOpenFileDialogService>();
            Container.RegisterType<ISaveFileDialogService, WpfSaveFileDialogService>();

            base.ConfigureContainer();
        }
    }
}
