﻿using System.Windows;
using Bespoke.Common;

namespace Bespoke.GestureClassifier.Wpf
{
    public class WpfMessageBoxService : IMessageBoxService
    {
        public void Show(string message, string caption = null)
        {
            MessageBox.Show(message, caption);
        }
    }
}
