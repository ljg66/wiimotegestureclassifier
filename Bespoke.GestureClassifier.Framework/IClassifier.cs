﻿namespace Bespoke.GestureClassifier.Framework
{
    public interface IClassifier
    {
        ClassifiedGestureCollection ClassifyGesture(Gesture gesture);
    }
}
