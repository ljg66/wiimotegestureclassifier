﻿namespace Bespoke.Common
{
    public interface IMessageBoxService
    {
        void Show(string message, string caption = null);
    }
}
