using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using Bespoke.Common;

namespace Bespoke.GestureClassifier.Framework
{
	public class MultiGesture
	{
		public string GestureName { get; set; }

		public Dictionary<PlayerIndex, TrainedGesture> TrainedGestures { get; }

		public MultiGesture(string gestureName)
		{
			Assert.ParamIsNotNull(nameof(gestureName), gestureName);
	
			GestureName = gestureName;
			TrainedGestures = new Dictionary<PlayerIndex, TrainedGesture>();
		}

		public static async Task<MultiGesture> LoadAsync(BinaryReader reader, TrainedGestureSet trainedGestureSet)
		{
            MultiGesture multiGesture = null;
            await Task.Run(() =>
            {
                string gestureName = reader.ReadString();
                multiGesture = new MultiGesture(gestureName);

                int totalTrainedGestures = reader.ReadInt32();
                for (int i = 0; i < totalTrainedGestures; i++)
                {
                    PlayerIndex referencedPlayerIndex = (PlayerIndex)Enum.Parse(typeof(PlayerIndex), reader.ReadString());
                    string referencedGestureName = reader.ReadString();
                    TrainedGesture trainedGesture = trainedGestureSet.TrainedGestureCollections[referencedPlayerIndex][referencedGestureName];
                    if (trainedGesture == null)
                    {
                        throw new Exception(string.Format("Could not find referenced gesture [{0}:{1}] in MultiGesture [{2}]", referencedPlayerIndex, referencedGestureName, multiGesture.GestureName));
                    }
                    
                    multiGesture.TrainedGestures.Add(trainedGesture.PlayerIndex, trainedGesture);
                }
            });

			return multiGesture;
		}

		public async Task SaveAsync(BinaryWriter writer)
		{
            await Task.Run(() =>
            {
                writer.Write(GestureName);
                writer.Write(TrainedGestures.Count);
                foreach (TrainedGesture trainedGesture in TrainedGestures.Values)
                {
                    writer.Write(trainedGesture.PlayerIndex.ToString());
                    writer.Write(trainedGesture.GestureName);
                }
            });
		}

        public override string ToString() => GestureName;

        public static bool operator ==(MultiGesture lhs, MultiGesture rhs)
        {
            if (ReferenceEquals(lhs, rhs))
            {
                return true;
            }

            if ((object)lhs == null || (object)rhs == null)
            {
                return false;
            }

            return (lhs.GestureName == rhs.GestureName);
        }

        public static bool operator !=(MultiGesture lhs, MultiGesture rhs) => !(lhs == rhs);

        public override bool Equals(object obj)
        {
            MultiGesture rhs = obj as MultiGesture;
            if ((object)rhs == null)
            {
                return false;
            }

            return this == rhs;
        }

        public override int GetHashCode() => GestureName.GetHashCode();
    }

	public class TrainedGestureSet
	{
		public Dictionary<PlayerIndex, TrainedGestureCollection> TrainedGestureCollections { get; }

		public ReadOnlyCollection<MultiGesture> MultiGestures => mMultiGestures.AsReadOnly();

		public TrainedGestureSet()
		{
			TrainedGestureCollections = new Dictionary<PlayerIndex, TrainedGestureCollection>();
			foreach (PlayerIndex playerIndex in Enum.GetValues(typeof(PlayerIndex)))
			{
				TrainedGestureCollections.Add(playerIndex, new TrainedGestureCollection(playerIndex));
			}

			mMultiGestures = new List<MultiGesture>();
		}

		public static async Task<TrainedGestureSet> LoadAsync(string fileName)
		{
			if (File.Exists(fileName) == false)
			{
				throw new FileNotFoundException();
			}

			using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
			{
				return await LoadAsync(new BinaryReader(fileStream));
			}
		}

		public static async Task<TrainedGestureSet> LoadAsync(byte[] trainingSet)
		{
			Assert.ParamIsNotNull("trainingData", trainingSet);

			using (MemoryStream memoryStream = new MemoryStream(trainingSet))
			{
				return await LoadAsync(new BinaryReader(memoryStream));
			}
		}

		private static async Task<TrainedGestureSet> LoadAsync(BinaryReader reader)
		{
			TrainedGestureSet trainedGestureSet = new TrainedGestureSet();

			foreach (PlayerIndex playerIndex in Enum.GetValues(typeof(PlayerIndex)))
			{
				PlayerIndex readPlayerIndex = (PlayerIndex)Enum.Parse(typeof(PlayerIndex), reader.ReadString());
				Assert.IsTrue("PlayerIndex", readPlayerIndex == playerIndex);
				
				trainedGestureSet.TrainedGestureCollections[playerIndex] = await TrainedGestureCollection.LoadAsync(reader, playerIndex);
			}			

			int totalMultiGestures = reader.ReadInt32();
			for (int i = 0; i < totalMultiGestures; i++)
			{
				MultiGesture multiGesture = await MultiGesture.LoadAsync(reader, trainedGestureSet);
				trainedGestureSet.AddMultiGesture(multiGesture);
			}

			return trainedGestureSet;
		}

		public async Task SaveAsync(string fileName)
		{
			using (FileStream file = new FileStream(fileName, FileMode.Create, FileAccess.Write))
			{
				BinaryWriter writer = new BinaryWriter(file);

				foreach (TrainedGestureCollection trainedGestureCollection in TrainedGestureCollections.Values)
				{
					writer.Write(trainedGestureCollection.PlayerIndex.ToString());
					await trainedGestureCollection.SaveAsync(writer);
				}

				writer.Write(mMultiGestures.Count);
				foreach (MultiGesture multiGesture in mMultiGestures)
				{
					await multiGesture.SaveAsync(writer);
				}
			}
		}

		public void AddMultiGesture(MultiGesture multiGesture)
		{
			Assert.ParamIsNotNull(nameof(multiGesture), multiGesture);

			mMultiGestures.Add(multiGesture);
		}

		public void RemoveMultiGesture(MultiGesture multiGesture)
		{
            Assert.ParamIsNotNull(nameof(multiGesture), multiGesture);

            mMultiGestures.Remove(multiGesture);
		}

		private List<MultiGesture> mMultiGestures;
	}
}
